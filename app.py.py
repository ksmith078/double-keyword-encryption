from tkinter import Tk
from tkinter.filedialog import askopenfilename
run = 'yes'

while run == 'yes':

    print("This program will only process alphabetic text")

    def getMode():
        while True:
            print("Do you wish to encrypt[1] or decrypt[2]")
            mode = input("Please enter '1' or '2': ")
            if mode == '1' or mode == '2':
                return mode
            else:
                print("Invalid")
            



    def getMessage():
        message = ''
        while True:
            message = input("Type '@' to open file browser and select text file or type message here: ").upper()
            if message != '@':
                filepath = ''
                print("Your message is", message)
                return (message, filepath)
            else:
                try:
                    Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
                    print("Please select the text file you wish to use")
                    filepath = askopenfilename()
                    print(filepath)
                    file = open(filepath, 'r')
                    message = file.read().upper()
                    return (message, filepath)
                except:
                    print("Error reading text file")
                
        



    def keyword():
        while True:
            keyword = input(keywordAttempt + " keyword: ")
            if keyword.isalpha() == True:
                print(keywordAttempt, "keyword accepted!")
                return keyword
            else:
                print(keywordAttempt, "keyword invalid, try again")


    def makeList(letterList, keyword):
        counter = 0
        for char in keyword:
            letter = keyword[counter]
            letterList.append(letter)
            counter += 1
        return letterList

    def crypt():
        newMessage = ''
        counter = 0
        counter2 = 0
        for char in message:
            if char.isalpha():
                num = ord(char)
                num2 = ord(letterList[counter])
                num3 = ord(letterList2[counter2])
                if mode == '1':
                    newNum = num + num2 + num3 - 128
                elif mode == '2':
                    newNum = num - num2 - num3 + 128
                while newNum > ord('Z'):
                    newNum -= 26
                while newNum < ord('A'):
                    newNum += 26
                newChar = chr(newNum)
                newMessage += newChar
                counter += 1
                counter2 +=1
                if counter == len(userKeyword):
                    counter = 0
                else:
                    counter = counter
                if counter2 == len(userKeyword2):
                    counter2 = 0
                else:
                    counter = counter
            else:
                newMessage += char
        return newMessage

    def runagain():
        while True:
            run = input("Would you like to run the program again? Enter 'yes' or 'no': ")
            if run == 'no':
                print("Bye")
                return run
            elif run == 'yes':
                return run
            else:
                print(run, "is invalid, try again")

        
    mode = getMode()
    (message, filepath) = getMessage()
    if message != '':
        keywordAttempt = 'First'
        userKeyword = keyword()
        keywordAttempt = 'Seconed'
        userKeyword2 = keyword()
        letterList = []
        letterList2 = []

        letterList = makeList(letterList, userKeyword)
        letterList2 = makeList(letterList2, userKeyword2)
        newMessage = crypt()
        if filepath != '':
            file = open(filepath, 'w')
            file.write(newMessage)
            file.close()

        if mode == '1':
            print("File has now been encrypted")
        elif mode == '2':
            print("File has now been decrypted")

        if filepath == '':
            print(newMessage)
    run = runagain()
    input("Press enter\n")